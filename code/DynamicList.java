
public class DynamicList<E> implements List<E> {

    private Object elements [];
    private int size = 0;
    private static final int DEFAULT_CAPACITY = 10;

    public DynamicList() {
        this.elements = new Object[DEFAULT_CAPACITY];
    }

    public void increaseCapacity() {
        Object[] newElements = new Object[this.elements.length + DEFAULT_CAPACITY];
        for (int index = 0; index < this.elements.length; index++) {
            newElements[index] = this.elements[index];
        }
        this.elements = newElements;
    }

    public boolean add(E e) {
        if( size == elements.length ) {
            this.increaseCapacity();
        }
        this.elements[this.size] = e;
        this.size++;
        return true;
    }

    public boolean remove(int index) {
        boolean response = false;
        E temp = (E) this.elements[index];
        this.deleteElement(index);
        E currentElementAfterDeletion = (E) this.elements[index];
        if (temp != currentElementAfterDeletion || temp == null){
            response = true;
        }
        return response;
    }

    public boolean remove(E e) {
        boolean response = false;
        int indexElement = this.searchElement(e);
        E temp = (E) this.elements[indexElement];
        this.deleteElement(indexElement);
        E currentElementAfterDeletion = (E) this.elements[indexElement];
        if (temp != currentElementAfterDeletion || temp == null){
            response = true;
        }
        return response;
    }

    public int searchElement(E e){
        for(int index=0; index<this.elements.length; index++) {
            if(e == this.elements[index]) {
                return index;
            }
        }
        return -1;
    }

    public boolean update(int index, E e) {
        boolean response = false;
        E temp = (E) this.elements[index];
        this.elements[index] = e;
        if (temp != this.elements[index]){
            response = true;
        }
        return response;
    }

    public E get(int index) {
        return (E) this.elements[index];
    }

    public Object[] getAll() {
        return this.elements;
    }

    private void deleteElement(int index){
        int list_size = this.elements.length;
        for (int i=index; i < list_size-1; i++)
            elements[i] = elements[i+1];
        list_size--;
    }

}
