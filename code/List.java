public interface List<E> {

    public boolean add(E e);

    public boolean remove(int index);

    public boolean remove(E e);

    public boolean update(int index, E e);

    public E get(int index);

}
