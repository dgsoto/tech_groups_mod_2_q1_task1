public class Main {

    public static void main(String[] args) {
        DynamicList<String> list = new DynamicList<String>();
        list.add("Uno");
        list.add("Dos");
        list.add("3");
        list.add("4");
        list.add("5");
		list.add("6");
		list.add("7");
		list.add("8");
		list.add("9");
		list.add("10");
		list.add("11");
        list.add("12");
        list.add("java");
        list.add("python");
        list.add("php");

        Object[] all = list.getAll();
        for(int index = 0; index < all.length; index++) {
            System.out.println(all[index]);
        }

        System.out.println(list.update(0, "5Unoss0"));
        System.out.println(list.get(0));
        System.out.println(list.remove(7));
        System.out.println(list.remove("python"));

        for(int i=0; i<list.getAll().length ; i++){
            System.out.println(list.get(i) + " --- index: " + i);
        }
    }
}
