# Tech_groups_Mod_2_Q1_Task1

This is the first task

## Result of the execution of Main.java

**Output without changes**
```
Uno
Dos
3
4
5
6
7
8
9
10
11
12
java
python
php
null
null
null
null
null
```

**Output with changes**
```
5Unoss0 --- index: 0
Dos --- index: 1
3 --- index: 2
4 --- index: 3
5 --- index: 4
6 --- index: 5
7 --- index: 6
9 --- index: 7
10 --- index: 8
11 --- index: 9
12 --- index: 10
java --- index: 11
php --- index: 12
null --- index: 13
null --- index: 14
null --- index: 15
null --- index: 16
null --- index: 17
null --- index: 18
null --- index: 19
```
